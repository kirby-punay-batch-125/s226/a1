const courses = [
	{
		id: "1",
		name: "Mathematics",
		description: "math",
		price: 1000,
		isActive: true,
	},
	{
		id: "2",
		name: "English",
		description: "eng",
		price: 100,
		isActive: true,
	},
	{
		id: "3",
		name: "Science",
		description: "sci",
		price: 10000,
		isActive: true,
	},
	{
		id: "4",
		name: "Filipino",
		description: "fil",
		price: 500,
		isActive: true,
	},
];

const addCourse = (id, name, desc, price)  => {
	let subject = {
	id: id,
	name: name,
	description: desc,
	price: price,
	isActive: true,
	};
	courses.push(subject);
	alert(`You have created ${name}. It's price is ${price}.`);
};

addCourse("5","Religion","rel",500,true)
console.log(courses)

const findCourse = (idNum) => {
 for(let i = 0; i < courses.length; i++){
   if(courses[i].id == idNum){
     return courses[i]
   }
 }
}
console.log(findCourse("2"));


const getAllCourses = courses.map((course) => {console.log(course)});

const archiveCourse = (idNum, status) => {
 for(let i = 0; i < courses.length; i++){
   if(courses[i].id == idNum){
     courses[i].isActive = status
   }
 }
}
archiveCourse("1", false)
console.log(courses)

const deleteCourse = courses.pop((course) => {console.log(course)});
